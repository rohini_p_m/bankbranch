package Main;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface BranchService
{
	

	// this will display all the branch list
	void display();

	/*
	 * This method will add a branch
	 * 
	 * @param add branch  to be added
	 */
	void add(Branch branch1);
	
	//will search for a branch using name
	void search(String name);
	
	//will create a list
	List<Branch> search(LocalDate branchOpendate);
	
	
	
	void sort();
	
	
	
	//This method will delete the name,according to the searched name 
	void delete(String name);
	
    //will convert details in a CSV file
	void downloadCSV()throws IOException;
	default String convertTocsv(Branch branch) 
	{

		StringBuffer branchh=new StringBuffer();
	 	branchh.append(branch.getName());
	 	branchh.append(",");
	 	branchh.append(branch.getiFSCCode());
	 	branchh.append(",");
	 	branchh.append(branch.getPlace());
	 	branchh.append(",");
	 	branchh.append(branch.getNumOfEmployees());
	 	branchh.append(",");
	 	branchh.append(branch.getCreditCards());
	 	branchh.append(",");
	 	branchh.append(branch.getBranchOpenDate());
	 	return branchh.toString();
	 
	}
	
	 //will convert details in a Json file
	
	void downloadJson()throws IOException;

	default String getJson(Branch branch) {

		return new StringBuffer().append("{").append("\"Name\":").append("\"").append(branch.getName())
		.append("\"").append(",").append("\"IFSC Code\":").append(branch.getiFSCCode()).append(",").append("\"Place\":")
		.append("\"").append(branch.getPlace()).append("\"").append(",").append("\"Branch Open date\":").append("\"")
		.append(branch.getBranchOpenDate()).append("}").append("\n").toString();



		}
		
	}
	
