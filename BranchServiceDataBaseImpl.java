package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class BranchServiceDataBaseImpl implements BranchService {
	
	String driver,url,user,password;


public BranchServiceDataBaseImpl(String driver,String url,String user,String password)
	
{
	this.driver=driver;
	this.url=url;
	this.user=user;
	this.password=password;
	
	
}
private Connection getConnection() throws ClassNotFoundException, SQLException
{
	Class.forName(driver);
	return DriverManager.getConnection(url, user, password);
}
public void add(Branch branch)

{
	try {
		Connection connection=getConnection();
		String query="INSERT INTO bank(name,ifsccode,place,num_of_employees,branch_open_date,credi_cards)VALUES (?,?,?,?,?,?)";
		PreparedStatement stmt=connection.prepareStatement(query);
		stmt.setString(1, branch.getName());
		stmt.setString(2, branch.getiFSCCode());
		stmt.setString(3, branch.getPlace());
		stmt.setInt(4, branch.getNumOfEmployees());
		stmt.setDate(5, Date.valueOf(branch.getBranchOpenDate()));
		stmt.setString(6, branch.getCreditCards().toString());
		
		
		
		stmt.executeUpdate();
		connection.close();	}	
				
	catch(Exception e) {
		e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());	
			System.exit(0);	
	}


	System.out.println();
	System.out.println("Values added successfully");}
	
public void display() 
{
	try
	{
	Connection connection=getConnection();
	String query="SELECT * FROM bank  ";
	PreparedStatement st=connection.prepareStatement(query);
	
      
    ResultSet rset=st.executeQuery();
    while(rset.next())
    {
    	

		Branch branch=getBranch(rset);
		System.out.println(branch);
		
    	
   	
   	 
    }
    	st.close();
    	rset.close();
    	connection.close();
	}catch(Exception e)
	{
	e.printStackTrace();
	System.err.println( e.getClass().getName()+": "+ e.getMessage() );
    System.exit(0);
 }
	System.out.println();
	System.out.println("VALUES ARE SUCESSFULLY DISPLAYED");
}



private Branch getBranch(ResultSet rset) {
	// TODO Auto-generated method stub
	return null;
}
public void search(String name)
{
	try {
		Connection cn ;
		cn = getConnection();
		PreparedStatement preparedStatement = cn.prepareStatement("SELECT * FROM bank where name=?");
		preparedStatement.setString(1, name);
		ResultSet rset = preparedStatement.executeQuery();
		while (rset.next())

		{
			Branch branch=getBranch(rset);
			System.out.println(branch);
			}
	
   	 System.out.println("\n");
   	
	cn.close();



	} catch (ClassNotFoundException | SQLException e) {
	e.printStackTrace();
	}



}
public void sort()
{	try {
	Connection connection;
	connection = getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bank order by name");
	ResultSet resultSet = preparedStatement.executeQuery();
	while (resultSet.next())



	{
	Branch branch = getBranch(resultSet);
	System.out.println(branch);



	}
	resultSet.close();
	preparedStatement.close();
	connection.close();



	} catch (Exception e) {
	e.printStackTrace();
	System.err.println(e.getClass().getName() + ": " + e.getMessage());
	System.exit(0);
	}
	System.out.println("Sort displayed");



	}

public List<Branch> search(LocalDate date)
{
List<Branch>branchlist = new ArrayList<Branch>();
try {
	Connection cn ;
	cn = getConnection();
	PreparedStatement preparedStatement = cn.prepareStatement("SELECT * FROM bank where branch_open_date?");
	preparedStatement.setDate(1,Date.valueOf(date));
	ResultSet rset = preparedStatement.executeQuery();
	while (rset.next())

	{
		Branch branch=getBranch(rset);
		System.out.println(branch);
	}
	cn.close();


	
	} catch (ClassNotFoundException | SQLException e) {
	e.printStackTrace();
	}



return (branchlist);

}

public void delete(String name)
{

try {
	Connection cn ;
	cn = getConnection();
	PreparedStatement preparedStatement = cn.prepareStatement("DELETE FROM bank where name=?");
	preparedStatement.setString(1, name);
	
	
	preparedStatement.executeUpdate();
	System.out.println("ELEMENT DELETED");
	
	cn.close();
	//display(); 
	
	



	} catch (ClassNotFoundException | SQLException e) {
	e.printStackTrace();
	}

}

public void downloadCSV()throws IOException 

{

Connection connection;
try {
connection = getConnection();
PreparedStatement pstmt = connection.prepareStatement("select * from bank");
ResultSet resultSet = pstmt.executeQuery();
FileWriter file = new FileWriter("download.csv");
while (resultSet.next()) {

String data =convertTocsv(getBranch(resultSet)).toString();
file.write(data);
}
file.close();
} catch (ClassNotFoundException e) {
// TODO Auto-generated catch block
e.printStackTrace();
} catch (SQLException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}

}


public void downloadJson()throws IOException
		{

Connection connection;
	
	
	
	try {
	connection = getConnection();
	PreparedStatement pstmt = connection.prepareStatement("select * from bank");
	ResultSet resultSet = pstmt.executeQuery();
	FileWriter file = new FileWriter("download1.json");
	StringJoiner join = new StringJoiner(",", "[", "]");
	while (resultSet.next()) {
	String data = join.add(getJson(getBranch(resultSet))).toString();
	file.write(data);
	
	
	
	}
	file.close();
	connection.close();
	} catch (ClassNotFoundException | SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}



}
}
