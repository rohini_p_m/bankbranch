package Main;


import java.io.FileWriter;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.Iterator;

public  class BranchServiceArrayListImpl implements BranchService{
	
	List <Branch> branchlist=new ArrayList<>();
	
	
		@Override
		public void add(Branch branch) {
			if(branchlist.size()>0 && branchlist.contains(branch))
			{
				System.out.println("DATA ALREADY EXSIST.");
			}
			else
			{

				branchlist.add(branch);
					
			}	
		}


		@Override
		public void display() {

			
			branchlist.forEach(System.out::print);
		
		}
		public void search(String name)
		{
			branchlist.stream()
		      .filter(str -> str.getName().equals(name))
		      .collect(Collectors.toList());
			
			
			System.out.println(branchlist);
				/*for(Branch search : branchlist) {
					
					if(search.getName().equalsIgnoreCase(name)) {
					System.out.println(search);*/
			      
		}
		
	public void sort()
		
		{
			
			Collections.sort( branchlist);
			
//			display();
		}
		
	
		
		public List<Branch> search(LocalDate branchOpendate)
{
			
			return branchlist.stream().filter(date -> date.getBranchOpenDate().isAfter(branchOpendate)).collect(Collectors.toList());
			//return branchlist;
		}
		
		public void delete(String name)
		{
			
			
				// TODO Auto-generated method stub
				
//				
			for(Iterator<Branch> itr = branchlist.iterator();itr.hasNext();) {
				Branch name1=itr.next();
				if (name1.getName().equals(name))
				{
				itr.remove();
				System.out.println("BRANCH DELETED SUCESSFULLY");
				}

				}
				}

			
		
		
		public void downloadCSV()throws IOException {
			
			
			
				FileWriter file=new FileWriter("download.csv");
//				Branch[] branchlist;
				for(Branch branchh : branchlist)
				{
					file.write(convertTocsv(branchh));
				}
					file.close();
			
		
		}
			 
					
		
		public void downloadJson()throws IOException
		{
			
			
				StringJoiner join = new StringJoiner(",", "[", "]");
				String data;
				FileWriter fileWriter = new FileWriter("downloadJson.json");
				for (Branch branch : branchlist) 
				{
				data = getJson(branch);
				join.add(data);
				}
				// System.out.println(join.toString());
				fileWriter.write(join.toString());



				fileWriter.close();
		}

				
			}
