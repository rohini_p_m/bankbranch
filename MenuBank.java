package Main;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;

import Main.Branch;

public class MenuBank {

	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		FileReader reader=new FileReader("src/config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		
		int count = 0;int choice;
		BranchService branchService=BranchServiceFactory.getBranchService(properties); 
		do {
					System.out.println(
					" 1. Insert Data \n 2. Display Data \n 3.Search name \n 4. search date \n 5.Sort \n 6.Download details as CSV format \n 7.Download details as Json format \n 8.Delete \n 9.Exit ");
					System.out.println();
					System.out.println("Enter your Choice ");
					choice = scanner.nextInt();
					CreditCards creditcards;
					 System.out.println();
					switch (choice) {
			case 1:
				try {

					System.out.println();
					System.out.println("Enter your Details");
					System.out.println();
					System.out.println("Enter the Bank name");
					String name = scanner.next();
					System.out.println("Enter IFSC Code");
					String iFSCCode = scanner.next();
					System.out.println("Enter the place");
					String place = scanner.next();
					System.out.println("Enter number of employees");
					int numOfEmployees = scanner.nextInt();
					System.out.println("Enter the Branch open Date");
					
					LocalDate branchOpenDate = LocalDate.parse(scanner.next());
					System.out.println("Enter the credit card type");
					creditcards=CreditCards.valueOf(scanner.next().toUpperCase());
					Branch branch=new Branch (name,iFSCCode, place, branchOpenDate, numOfEmployees,branchOpenDate, creditcards);
					branchService.add(branch);
					System.out.println();
					break;
					}
					catch (Exception exception) {
					System.out.println("Something went wrong in CASE 1:" + exception.toString());
					}

			case 2:
				
//					System.out.println();
					branchService.display();
					break;
					
			case 3:
				
					System.out.println("Enter your Search name");
					String name = scanner.next();
					branchService.search(name);
					break;
					
			case 4: 
					
					System.out.println("Enter the search date");
					String searchBranchOpenDate = scanner.next();
					LocalDate BranchOpenDate = LocalDate.parse(searchBranchOpenDate);
					branchService.search(BranchOpenDate);
					break;
					

			case 5:
				
					System.out.println();
					branchService.sort();
					break;
					
			case 6:
				try {
					
					System.out.println();
					System.out.println("YOUR RECORDS ARE DOWNLOADED AS CSV FILE");
					branchService.downloadCSV();
					}
					catch (Exception e) {
					System.out.println("Exception");
					e.printStackTrace();
					}
					break;
			case 7:	
				try {
					System.out.println();
					System.out.println("YOUR RECORDS ARE DOWNLOADED AS Json FILE");
					branchService.downloadJson();
					}
					catch (Exception e) {
					System.out.println("Exception");
					e.printStackTrace();
					}
					break;
				
					
			case 8:	
				
					System.out.println();
					System.out.println("Enter the name to be deleted");
					String name1=scanner.next();
					branchService.delete(name1);
					break;
					
			case 9:
					System.exit(0);
					break;
					
			default:
					System.out.println("Invalid Entry");

			
			}}
					while(choice!=7);	}
	}	
		

	
