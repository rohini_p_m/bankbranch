package Main;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Scanner;

public class Branch implements Comparable<Branch> {
	private String name;
	private String iFSCCode;
	private String place;
	private LocalDate branchOpenDate;
	private int numOfEmployees;
	private CreditCards creditCards;

	public Branch(String name, String iFSCCode, String place,LocalDate branchOpendate, int numOfEmployees, LocalDate branchOpenDate,CreditCards creditCards) {
		this.name = name;
		this.branchOpenDate =branchOpenDate ;
		this.iFSCCode = iFSCCode;
		this.numOfEmployees = numOfEmployees;
		this.place = place;
		this.creditCards=creditCards;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getiFSCCode() {
		return iFSCCode;
	}

	public void setiFSCCode(String iFSCCode) {
		this.iFSCCode = iFSCCode;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public LocalDate getBranchOpenDate() {
		return branchOpenDate;
	}

	public void setBranchOpenDate(LocalDate branchOpenDate) {
		this.branchOpenDate = branchOpenDate;
	}

	public int getNumOfEmployees() {
		return numOfEmployees;
	}

	public void setNumOfEmployees(int numOfEmployees) {
		this.numOfEmployees = numOfEmployees;
	}
	public CreditCards getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(CreditCards CreditCards) {
		this.creditCards = creditCards;
	}

	void display() {
		System.out.println(name);
		System.out.println(iFSCCode);
		System.out.println(place);
		System.out.println(numOfEmployees);
		System.out.println(branchOpenDate);
		System.out.println("This branch has ATM Facility? ");
		}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		return creditCards == other.creditCards && Objects.equals(branchOpenDate, other.branchOpenDate)
				&& Objects.equals(iFSCCode, other.iFSCCode) && Objects.equals(name, other.name)
				&& numOfEmployees == other.numOfEmployees && Objects.equals(place, other.place);
	}

	public String toString() {
		{
			
			return  new StringBuffer("NAME :  ").append(name).append("\nIFSC : ").append(iFSCCode).append("\nPlace : ")
					.append(place).append("\nNum of Employees : ").append(numOfEmployees).append("\nBranch open Date : ").append(branchOpenDate).toString();
}
	}

	public int compareTo(Branch branch) {

		
		
			return (numOfEmployees - branch.numOfEmployees);
	}
}
