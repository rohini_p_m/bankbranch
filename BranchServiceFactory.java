package Main;

import java.util.Properties;

public abstract class BranchServiceFactory {
	public static BranchService getBranchService(Properties properties)
	{
		String type=properties.getProperty("implementation");
//		type=type.toUpperCase();
		BranchService service=null;
		switch(type) {
		case "Array":
			service=new BranchServiceImpl();
			break;
			
		case "ArrayList":
			service=new BranchServiceImpl();
			break;
			
		case "database":
			String driver=properties.getProperty("driver");
			String url=properties.getProperty("url");
			String user=properties.getProperty("user");
			String password=properties.getProperty("password");
			service=new BranchServiceDataBaseImpl(driver,url,user,password);
		}
	return service;
		
		
	}

}

